Quando('faco a requisicao do segmento') do
    seg = Segmento.new
    @lista_seg = seg.listar_segmentos($autenticacao)
end

Entao('valido o retorno do segmento') do
    expect(@lista_seg.code).to eq(200)
    @lista_seg['segmentos'].each { |row| expect(!row['id'].nil?).to be_truthy }
    @lista_seg['segmentos'].each { |row| expect(!row['nome'].nil?).to be_truthy }
end