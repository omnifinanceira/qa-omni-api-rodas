Quando('faco a requisicao da listagem do banco') do
    banco = Banco.new
    @lista_bancos = banco.listar_bancos($autenticacao)
end

Entao('valido o retorno da listagem do banco') do
    expect(@lista_bancos.code).to eq(200)
    expect(@lista_bancos['status']).to eq("SUCCESS")
    @lista_bancos['bancos'].each { |row| expect(!row['codigo'].nil?).to be_truthy }
    @lista_bancos['bancos'].each { |row| expect(!row['descricao'].nil?).to be_truthy }
end