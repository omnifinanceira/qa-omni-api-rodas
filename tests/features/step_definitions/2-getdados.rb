Quando('faco a simulacao para validar o get dados') do
    sim = Simular.new
    @response_simulador = sim.fazer_simulacao("","",$autenticacao)
end

Quando('faco a requisicao no getdados') do
    @gtd = GetDados.new
    @response_getdados = @gtd.get_dados(@response_simulador['proposta']['id'],$autenticacao)
end

Entao('valido o retorno do getdados') do

    while @response_getdados['status'] == "AGUARDANDO_CARREGAMENTO"
        @response_getdados = @gtd.get_dados(@response_simulador['proposta']['id'],$autenticacao)
    end

    expect(@response_getdados['status']).to eq("SUCCESS")
    expect(@response_getdados['message']).to eq("Agora é negociar com o seu cliente")
end