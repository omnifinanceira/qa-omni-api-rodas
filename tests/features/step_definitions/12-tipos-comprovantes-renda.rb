Quando('faco a requisicao do tipos comprovantes renda') do
    tipocomprovantesrenda = TiposComprovantesRenda.new
    @tipo_comprovantes_renda = tipocomprovantesrenda.listar_tipos_comprovantes_renda($autenticacao)
end

Entao('valido o retorno do tipos comprovantes renda') do
    expect(@tipo_comprovantes_renda.code).to eq(200)
    @tipo_comprovantes_renda['comprovantes'].each { |row| expect(!row['codGrupoParametro'].nil?).to be_truthy }
    @tipo_comprovantes_renda['comprovantes'].each { |row| expect(!row['chaveParametro'].nil?).to be_truthy }
    @tipo_comprovantes_renda['comprovantes'].each { |row| expect(!row['valorParametro'].nil?).to be_truthy }
    @tipo_comprovantes_renda['comprovantes'].each { |row| expect(!row['habilita'].nil?).to be_truthy }
end