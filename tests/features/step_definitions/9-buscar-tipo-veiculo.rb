Quando('faco a requisicao do tipo veiculo') do
    tipoveiculo = BuscarTipoVeiculo.new
    @lista_tp_veiculo = tipoveiculo.listar_tipo_veiculo($autenticacao)
end

Entao('valido o retorno do tipo veiculo') do
    expect(@lista_tp_veiculo.code).to eq(200)
    @lista_tp_veiculo['tipos'].each { |row| expect(!row['id'].nil?).to be_truthy }
    @lista_tp_veiculo['tipos'].each { |row| expect(!row['descricao'].nil?).to be_truthy }
end