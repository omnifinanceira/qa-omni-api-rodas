class FinalizarProposta < Request


    def finalizar_proposta(getdados_return,autenticacao)
        # binding.pry
        body = {
            "proposta":{
               "seguros":[
                  {
                     "id":8,
                     "valor":876.28
                  }
               ],
               "assistencias":[
                  {
                     "id":2,
                     "valor":200
                  }
               ],
               "id":getdados_return['proposta']['id'],
               "carencia":0,
               "valorEntrada":0,
               "valorTotal":13502,
               "valorLiquido":13502,
               "percRetorno":0,
               "valorParcela":608.11,
               "qtdParcelas":48,
               "fator":0.0362,
               "taxaProposta":2.4,
               "valorRegistro":0,
               "valorSircof":135.42,
               "valorTc":200,
               "valorDv":1615,
               "valorRetorno":0,
               "cliente":{
                  "id":getdados_return['cliente']['id'],
                  "cpf":getdados_return['cliente']['cpf'],
                  "nome":getdados_return['cliente']['nome'],
                  "renda":10000,
                  "email":gerar_email,
                  "nomeMae":gerar_nome,
                  "nomePai":"NAO DECLARADO",
                  "idTipoDocumento":1,
                  "rgIe":"123456789",
                  "rgIeDt":1556679600000,
                  "rgIeEmissor":"SSP",
                  "idEstadoCivil":"1",
                  "idNacionalidade":"1",
                  "naturalDe":"ANADIA",
                  "naturalDeUf":"AL",
                  "clientePpe":"N",
                  "valorPatrimonio":"0.01",
                  "telefones":[
                     {
                        "idCategoria":getdados_return['cliente']['telefones'][0]['idCategoria'],
                        "ddd":getdados_return['cliente']['telefones'][0]['ddd'],
                        "numero":getdados_return['cliente']['telefones'][0]['numero'],
                        "idTipoTelefone":getdados_return['cliente']['telefones'][0]['idTipoTelefone']
                     }
                  ],
                  "enderecos":[
                    #  {
                    #     "idTipoEndereco":getdados_return['cliente']['enderecos'][0]['idTipoEndereco'],
                    #     "uf":getdados_return['cliente']['enderecos'][0]['uf'],
                    #     "cidade":getdados_return['cliente']['enderecos'][0]['cidade'],
                    #     "endereco":getdados_return['cliente']['enderecos'][0]['endereco'],
                    #     "bairro":getdados_return['cliente']['enderecos'][0]['bairro'],
                    #     "numero":getdados_return['cliente']['enderecos'][0]['numero'],
                    #     "cep":getdados_return['cliente']['enderecos'][0]['cep']
                    #  }
                    {
                        "idTipoEndereco":1,
                        "uf":"SP",
                        "cidade":"SANTO ANDRE",
                        "endereco":"RUA MARTINHO PRADO",
                        "bairro":"JARDIM CRISTIANE",
                        "numero":"1",
                        "cep":"09185200"
                     }
                  ],
                  "tipoEnderecoCorresp":"1",
                  "dadoProfissional":{
                     "idClasseProfissional":9,
                     "idProfissao":932
                  },
                  "clienteReferencia":{
                     "nome":"NOME DA REFERENCIA",
                     "idGrauRelacionamento":"1",
                     "telefone":{
                        "ddd":"11",
                        "numero":"985491564"
                     }
                  }
               },
               "avalistas":[
               ],
               "portarDivida":"N"
            }
         }

        binding.pry
        finalizar = exec_post("/veiculo/finalizarSimulacao",body,autenticacao)

        return finalizar
    end
end