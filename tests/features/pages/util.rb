require_relative '../pages/geradorrandomico.rb'

class Util < GeradorRandomico

    def initialize
        $usuario = "331LUANA"
        $senha = "SENHA123"
        $autenticacao = ""
        criar_pasta_log
    end
    # {"login":"184BATISTELLA","token":"56471609","agente":184}
    def criar_pasta_log
        @diretorio = "C:/report_automacao"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end

    def gravar_request_response(path,request,response)
        arquivo = "C:/report_automacao/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"

        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo.close
        
        arquivo2 = "C:/bitbucket/refatoracao-testes-automatizados/ref-omni-api/tests/reports/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo2 = File.new(arquivo2, "w")
        File.write(arquivo2, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo2.close
    end

    def gravar(request_return)
        arquivo = "C:/report_automacao/#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "#{request_return.to_json.force_encoding("UTF-8")}")
        arquivo.close
    end
end