class TipoTransportes < Request

    def listar_tipos_transportes(autenticacao)

        tipostransportes = exec_get("/tipos-transportes",autenticacao,"https://dev-geral.omni.com.br/api")

        return tipostransportes
    end
end