class Simular < Request

    def fazer_simulacao(cpf,placa,autenticacao)

        # body = {
        #     "grupo1": "CDC",
        #     "cpf": cpf,
        #     "lojaId": "42556",
        #     "vendedorId": "0",
        #     "dataNascimento": 605502000000,
        #     "ddd": "11",
        #     "celular": gerar_telefone_celular.gsub('-',''),
        #     "renda": gerar_numero(10000,15999).to_s + '00',
        #     "veiculos":[
        #        {
        #           "placa": placa,
        #           "uf": "SP",
        #           "utilizacaoAgropecuaria": false,
        #           "condicao": "USADO"
        #        }
        #     ],
        #     "plataformaInclusao": "WEB",
        #     "tempoGastoFicha": 43378
        #  }

        body = {
            "grupo1": "CDC",
            "cpf": "65230566000",
            "lojaId": "42556",
            "vendedorId": "0",
            "dataNascimento": 605502000000,
            "ddd": "11",
            "celular": "970250500",
            "renda": "9000.00",
            "veiculos":[
               {
                  "placa": "DUI5206",
                  "uf": "SP",
                  "utilizacaoAgropecuaria": false,
                  "condicao": "USADO"
               }
            ],
            "plataformaInclusao": "WEB",
            "tempoGastoFicha": 43378
         }

        simulacao = exec_post("/veiculo/simular",body,autenticacao)

        return simulacao
    end
end