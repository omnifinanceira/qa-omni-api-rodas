class ClasseProfissional < Request

    def listar_classe_profissional(autenticacao)

        classeprofissional = exec_get("/classes-profissionais",autenticacao,"https://dev-geral.omni.com.br/api")

        return classeprofissional
    end
end