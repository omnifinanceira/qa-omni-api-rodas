class TiposComprovantesRenda < Request

    def listar_tipos_comprovantes_renda(autenticacao)

        produtosporusuariorefi = exec_get("/tipos-comprovantes-renda",autenticacao,"https://dev-geral.omni.com.br/api")

        return produtosporusuariorefi
    end
end