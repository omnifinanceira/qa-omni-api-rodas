class ProdutosPorUsuarioRefi < Request

    def listar_produtos_por_usuario_refi(autenticacao)

        produtosporusuariorefi = exec_get("/operacoes/produtos-por-usuario/REFINANCIAMENTO",autenticacao,"https://dev-geral.omni.com.br/api")

        return produtosporusuariorefi
    end
end