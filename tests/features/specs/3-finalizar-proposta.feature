#language:pt
@3 @finalizarproposta
Funcionalidade: Finalizar proposta no Omni Mais

@finalizarproposta
Cenario: Finalizar proposta

Quando eu autentico
E faco a simulacao para validar o finalizar proposta
E faco a requisicao no getdados finalizar proposta
E finalizo a proposta
Entao valido o retorno do finalizar proposta