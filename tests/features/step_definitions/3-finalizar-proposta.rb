Quando('faco a simulacao para validar o finalizar proposta') do
    sim = Simular.new
    @response_simulador = sim.fazer_simulacao("","",$autenticacao)
end

Quando('faco a requisicao no getdados finalizar proposta') do
    gtd = GetDados.new
    @response_getdados = gtd.get_dados(@response_simulador['proposta']['id'],$autenticacao)

    while @response_getdados['status'] == "AGUARDANDO_CARREGAMENTO"
        @response_getdados = gtd.get_dados(@response_simulador['proposta']['id'],$autenticacao)
    end

    expect(@lista_bancos['status']).to eq("SUCCESS")
end

Quando('finalizo a proposta') do
    util = Util.new
    util.gravar(@response_getdados)
    finalizar = FinalizarProposta.new
    finalizar.finalizar_proposta(@response_getdados,$autenticacao)
end

Entao('valido o retorno do finalizar proposta') do
    binding.pry
end