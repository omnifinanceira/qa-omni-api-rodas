class CidadeUf < Request

    def listar_cidade_uf(autenticacao, uf)

        listarcidadeuf = exec_get("/api/cidade/listCidadeByUf/#{uf}",autenticacao,"https://dev-omnimais.omni.com.br/v2/api-docs")

        return listarcidadeuf
    end
end