class Segmento < Request

    def listar_segmentos(autenticacao)

        segmentos = exec_get("/segmentos",autenticacao,"https://dev-geral.omni.com.br/api")

        return segmentos
    end
end