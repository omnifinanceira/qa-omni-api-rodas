class Autenticacao < Request

    def fazer_autenticacao(usuario = $usuario,senha = $senha)

        body = {
            "p_nome": usuario,
            "p_senha": senha
        }

        autenticacao = exec_post_aut("desenv/pck_login.prc_valida_usuario",body)

        return autenticacao
    end

    def ws_auth_user(usuario = $usuario,senha = $senha)
        body = {
            "usuarioTO": {
                "plataforma": "WEB",
                "versaoAplicacao": "2.4.0",
                "idCodAplicacao": "1",
                "login": "#{usuario}",
                "senha": "#{senha}",
                "tokenPush": ""
            }
        }

        autenticacao = exec_post_aut_ws("omni-ws/api/auth/user",body)
    end
end