Quando('eu preencher o campo {string}') do |usuario|
  @aut = Autenticacao.new
  @usuario = usuario
end

Quando('o campo {string}') do |senha|
  @senha = senha
end

Quando('faco a requisicao') do
  @response = @aut.fazer_autenticacao(@usuario,@senha)
end

Quando('faco a requisicao do ws auth user') do
  @response = @aut.ws_auth_user(@usuario,@senha)
end

Entao('valido o retorno da autenticacao de agente unico') do
  @response.each { |row| expect(!row.nil?).to be_truthy }
end

Entao('valido o retorno da autenticacao de mais de um agente') do
  binding.pry
  @response.each { |row| expect(!row.nil?).to be_truthy }
end

Quando('eu autentico') do
  aut = Autenticacao.new
  $autenticacao = aut.fazer_autenticacao
end