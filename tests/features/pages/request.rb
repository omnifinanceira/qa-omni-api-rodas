require_relative '../pages/util.rb'

class Request < Util

    def initialize
        @base_uri = 'https://dev-omnifacil2.omni.com.br/'
        @base_uri_simular = 'https://dev-omnimais.omni.com.br/api'
    end

    def exec_post_aut(path,body)
        url_full = "#{@base_uri}#{path}"

        response = HTTParty.post(url_full, 
        :body => body,
        :headers => {'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/x-www-form-urlencoded' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

        return response
    end

    def exec_post_aut_ws(path,body)
        url_full = "#{@base_uri}#{path}"

        response = HTTParty.post(url_full, 
        :body => body.to_json,
        :headers => {'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8' })

        $cookie = response.headers['set-cookie']

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

        return response
    end
  
    def exec_post(path,body,autenticacao)
        url_full = "#{@base_uri_simular}#{path}"

        response = HTTParty.post(url_full,
        :body => body.to_json,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            'omni-autenticacao' => {"login": "#{autenticacao['nome']}", "token": "#{autenticacao['ide']}", "agente": "#{autenticacao['emp']}"}.to_json })

        unless response['Erros'].nil?
            gravar_request_response(url_full,body,response)
        end

      return response
    end

    def exec_get(path,autenticacao,url = @base_uri_simular)
        url_full = "#{url}#{path}"

        response = HTTParty.get(url_full,
        :headers => { 'Content-Type' => 'application/json;charset=UTF-8',
            'Accept' => 'application/json;charset=UTF-8',
            'omni-autenticacao' => {"login": "#{autenticacao['nome']}", "token": "#{autenticacao['ide']}", "agente": "#{autenticacao['emp']}"}.to_json })

        # unless response['Erros'].nil?
        #     gravar_request_response(url_full,response)
        # end

      return response
    end
end