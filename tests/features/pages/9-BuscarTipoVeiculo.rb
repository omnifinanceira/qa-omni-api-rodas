class BuscarTipoVeiculo < Request

    def listar_tipo_veiculo(autenticacao)

        tipoveiculo = exec_get("/veiculo/buscarTipoVeiculo",autenticacao,"https://dev-api.omni.com.br/rodas/v1/api")

        return tipoveiculo
    end
end