class GetDados < Request

    def get_dados(numero_proposta,autenticacao)

        getdados = exec_get("/proposta/#{numero_proposta}/dados",autenticacao,"https://dev-api.omni.com.br/rodas/v2/api")

        return getdados
    end
end